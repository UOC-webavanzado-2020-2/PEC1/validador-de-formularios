const form = document.getElementById("form");
const username = document.getElementById("username");
const email = document.getElementById("email");
const password = document.getElementById("password");
const password2 = document.getElementById("password2");

function showError(input, message) {
  const formControl = input.parentElement;
  formControl.className = "form-control error";
  const small = formControl.querySelector("small");
  small.innerText = message;
}

function showSuccess(input) {
  const formControl = input.parentElement;
  formControl.className = "form-control success";
}

function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}
//const re = /^\S+$/;
//modifiqué el método checkLength para evaluar la longitud del nombre de usuario utilizando regex
function checkLength(input, min, max) {
  const re = /^\S{6,15}$/;

  if (re.test(input.value)) {
    showSuccess(input);
  } else {
    showError(
      input,
      `${getFieldName(
        input
      )} debe tener mínimo ${min} y máximo ${max} caracteres`
    );
  }
}
/*
function checkLength(input, min, max) {
  if (input.value.length < min) {
    showError(
      input,
      `${getFieldName(input)} must be at least ${min} characters`
    );
  } else if (input.value.length > max) {
    showError(
      input,
      `${getFieldName(input)} must be less than ${max} characters`
    );
  } else {
    showSuccess(input);
  }
}
*/

//function for email
//js email regex
function checkEmail(input) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (re.test(input.value.trim())) {
    showSuccess(input);
  } else {
    showError(input, "Email is not valid");
  }
}

function checkPasswordBuild(input) {
  //const re = /^(?=(?:.*\d){1})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\S{4,}$/;
  const re = /^\S+$/; //no permita espacios en blanco
  const re1 = /(?=(?:.*\d){1})/; // 1 numero
  const re2 = /(?=(?:.*[A-Z]){1})/; //1 letra mayuscula
  const re3 = /(?=(?:.*[a-z]){1})/; //1 letra minuscula
  const re4 = /(?=.*[$@$!%*?&])/; //1 caracter especial

  //if (input.value.trim() === "") {
  if (!re.test(input.value)) {
    showError(input, "La constraseña no puede tener espacios en blanco");
  } else if (!re1.test(input.value.trim())) {
    showError(input, "La constraseña debe tener al menos 1 numero");
  } else if (!re2.test(input.value.trim())) {
    showError(input, "La constraseña debe tener al menos 1 Mayúscula");
  } else if (!re3.test(input.value.trim())) {
    showError(input, "La constraseña debe tener al menos 1 minúscula");
  } else if (!re4.test(input.value.trim())) {
    showError(input, "La constraseña debe tener al menos 1 caractér especial");
  } else {
    showSuccess(input);
  }
}

function checkPasswordMatch(input1, input2) {
  if (input1.value !== input2.value) {
    showError(input2, "Passwords do not match");
  }
}

function checkRequired(inputArray) {
  inputArray.forEach((element) => {
    console.log(element);
    if (element.value.trim() === "") {
      showError(element, `${getFieldName(element)} is Required`);
    } else {
      showSuccess(element);
    }
  });
}

form.addEventListener("submit", function (e) {
  e.preventDefault();

  checkRequired([username, email, password, password2]);
  checkLength(username, 6, 15);
  checkEmail(email);
  checkPasswordBuild(password);
  checkPasswordMatch(password, password2);

  //checkPasswordBuild(password2);
});
